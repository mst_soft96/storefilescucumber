package com.example.StoreFiles.controllers;


import com.example.StoreFiles.models.requests.UploadFile;
import com.example.StoreFiles.models.responses.ResponseModel;
import com.example.StoreFiles.services.FileService;
import com.example.StoreFiles.storages.StorageProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Base64;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static com.example.StoreFiles.GlobalVariables.APP_VERSION;

/**
 * @author Mostafa Moradi
 * @version 1.0.0
 * @since 2021/8/17
 */

@RestController
@RequestMapping("/api/" + APP_VERSION + "/files/")

public class FileController {

    private final StorageProperties storageProperties;
    private final FileService fileService;
    private final ResponseModel responseModel;

    @Autowired
    public FileController(StorageProperties storageProperties, FileService storeService, ResponseModel responseModel) {
        this.storageProperties = storageProperties;
        this.fileService = storeService;
        this.responseModel = responseModel;
    }

    @PostMapping(path = "/upload")
    public ResponseEntity<?> uploadFile(@Valid @RequestBody UploadFile file, BindingResult bindingResult) {

        if (!bindingResult.hasErrors()) {
            return fileService.storeFile(file);

        } else
        {
            List<String> errors = bindingResult.getAllErrors().stream().map(DefaultMessageSourceResolvable::getDefaultMessage).collect(Collectors.toList());
            return responseModel.responseModel(HttpStatus.BAD_REQUEST.value(), errors);

        }
    }

    @GetMapping(value = "/{id}")
    public void downloadFile(HttpServletResponse response, @PathVariable String id,@RequestParam Boolean base64) throws IOException {
        Path file = Paths.get(storageProperties.getLocation(), fileService.findFileById(id));
        if(base64)
            downloadFileInBase64Format(file,response);
        else
            downloadFileWithoutBase64Format(file,response);


    }
    private void downloadFileInBase64Format(Path file,HttpServletResponse response) throws IOException {
        response.setContentType("text/plain");
        OutputStream wrap = Base64.getEncoder().wrap(response.getOutputStream());
        FileInputStream fis = new FileInputStream(file.toFile());
        int bytes;
        byte[] buffer = new byte[2048];
        while ((bytes=fis.read(buffer)) != -1) {
            wrap.write(buffer, 0, bytes);
        }
        fis.close();
        wrap.close();
    }
    private void downloadFileWithoutBase64Format(Path file,HttpServletResponse response){

        if (Files.exists(file)) {
            try {
                Files.copy(file, response.getOutputStream());
                response.getOutputStream().flush();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }




}
