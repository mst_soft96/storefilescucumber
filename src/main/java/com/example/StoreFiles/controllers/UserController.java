package com.example.StoreFiles.controllers;


import com.example.StoreFiles.models.requests.UserModel;
import com.example.StoreFiles.models.responses.ResponseModel;
import com.example.StoreFiles.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static com.example.StoreFiles.GlobalVariables.APP_VERSION;


@RestController
@RequestMapping("/api/" + APP_VERSION + "/users/")
public class UserController {
    private final UserService userService;
    private final ResponseModel responseModel;


    @Autowired
    public UserController(UserService userService, ResponseModel responseModel) {
        this.userService = userService;
        this.responseModel = responseModel;
    }
    @PostMapping(path = "/register")
    public ResponseEntity<?> uploadFile(@Valid @RequestBody UserModel userModel, BindingResult bindingResult) {
        if(!bindingResult.hasErrors()){
           return userService.userSore(userModel);

        }else {
            System.out.println("***************************************");
            List<String> errors = bindingResult.getAllErrors().stream().map(DefaultMessageSourceResolvable::getDefaultMessage).collect(Collectors.toList());
            return responseModel.responseModel(HttpStatus.BAD_REQUEST.value(), errors);
        }


    }
    @GetMapping(path = "/user/{username}")
    public ResponseEntity<?> getUser(@PathVariable String username) {

        if(!username.isEmpty()){
            return userService.getUser(username);

        }else {
            return responseModel.responseModel(HttpStatus.BAD_REQUEST.value(), Collections.singletonList("username is null"));
        }


    }
}
