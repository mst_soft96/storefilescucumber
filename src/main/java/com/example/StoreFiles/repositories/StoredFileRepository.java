package com.example.StoreFiles.repositories;


import com.example.StoreFiles.models.StoredFile;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Mostafa Moradi
 * @version 1.0.0
 * @since 2021/8/17
 */

@Repository
public interface StoredFileRepository extends MongoRepository<StoredFile,String> {

}
