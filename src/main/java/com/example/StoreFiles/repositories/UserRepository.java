package com.example.StoreFiles.repositories;

import com.example.StoreFiles.models.User;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * @author Mostafa Moradi
 * @version 1.0.0
 * @since 2021/8/17
 */

public interface UserRepository extends MongoRepository<User, String> {

    User findByUsername(String username);
    User findUserByUsername(String username);

}
