package com.example.StoreFiles.models.responses;

import lombok.Data;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author Mostafa Moradi
 * @version 2.0.0
 * @since 2020/8/20
 */
@Data
@Component

public class ResponseModel {
    private int status;
    private List<String>errorList;
    public ResponseEntity<?> responseModel(int status ,List<String>errorList ){

        ResponseModel customResponse = new ResponseModel();
        customResponse.setStatus(status);
        customResponse.setErrorList(errorList);
        return ResponseEntity.status(status).body(customResponse);
    }
}
