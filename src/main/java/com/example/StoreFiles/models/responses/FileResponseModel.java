package com.example.StoreFiles.models.responses;

import lombok.Data;
import org.springframework.stereotype.Component;

@Data
@Component
public class FileResponseModel {
    private String id;
}
