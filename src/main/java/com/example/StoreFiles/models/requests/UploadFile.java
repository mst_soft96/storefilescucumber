package com.example.StoreFiles.models.requests;

import lombok.Data;

@Data
public class UploadFile {
    private String username;
    private String file;
}
