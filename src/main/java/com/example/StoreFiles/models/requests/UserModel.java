package com.example.StoreFiles.models.requests;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class UserModel {
    private String name;
    private String username;
    private String password;
   // private List<String> roles;
}
