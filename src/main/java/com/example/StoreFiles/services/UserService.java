package com.example.StoreFiles.services;


import com.example.StoreFiles.models.User;
import com.example.StoreFiles.models.requests.UserModel;
import com.example.StoreFiles.models.responses.ResponseModel;
import com.example.StoreFiles.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class UserService {
    private final UserRepository userRepository;
    private final ResponseModel responseModel;


    @Autowired
    public UserService(UserRepository userRepository, ResponseModel responseModel ) {
        this.userRepository = userRepository;
        this.responseModel = responseModel;
    }

    public ResponseEntity<?> userSore(UserModel userModel) {
        User user = new User(userModel.getName(), userModel.getUsername(), userModel.getPassword(), null);
        try {
            user=userRepository.save(user);
            return ResponseEntity.ok().body(user);

        } catch (Exception e) {
            return responseModel.responseModel(HttpStatus.BAD_REQUEST.value(), Collections.singletonList(e.getMessage()));
        }

    }

    public User getUserByUsername(String name) {

        return userRepository.findByUsername(name);
    }

    public void updateUserByUsername(String username, String fileId) {
        User user = getUserByUsername(username);
        try {
            List<String> filesId;
            if (user.getFilesId() == null)
                filesId = new ArrayList<>();
            else
                filesId = user.getFilesId();

            filesId.add(fileId);
            user.setFilesId(filesId);

        } catch (Exception exception) {
        }


        userRepository.save(user);


    }

    public ResponseEntity<?> getUser(String username) {
        try {
            return ResponseEntity.ok(userRepository.findByUsername(username));
        }catch (Exception exception){
            return responseModel.responseModel(HttpStatus.BAD_REQUEST.value(), Collections.singletonList(exception.getMessage()));
        }
    }
}
