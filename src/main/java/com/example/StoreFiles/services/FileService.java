package com.example.StoreFiles.services;


import com.example.StoreFiles.models.StoredFile;
import com.example.StoreFiles.models.requests.UploadFile;
import com.example.StoreFiles.models.responses.FileResponseModel;
import com.example.StoreFiles.models.responses.ResponseModel;
import com.example.StoreFiles.repositories.StoredFileRepository;
import com.example.StoreFiles.storages.StorageService;
import org.apache.commons.io.IOUtils;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Base64;
import java.util.Collections;
import java.util.Objects;

/**
 * @author Mostafa Moradi
 * @version 1.0.0
 * @since 2021/8/17
 */
@Service
public class FileService {
    private final ResponseModel responseModel;
    private final StoredFileRepository storedFileRepository;
    private final StorageService storageService;
    private final FileResponseModel fileResponseModel;
    private final UserService userService;

    @Autowired
    public FileService(ResponseModel responseModel, StoredFileRepository storedFileRepository, StorageService storageService, FileResponseModel fileResponseModel, UserService userService) {
        this.responseModel = responseModel;
        this.storedFileRepository = storedFileRepository;
        this.storageService = storageService;
        this.fileResponseModel = fileResponseModel;
        this.userService = userService;
    }

    public ResponseEntity<?> storeFile(UploadFile uploadFile) {
        try {
            MultipartFile file= (MultipartFile) convertBase64ToPdf(uploadFile.getFile());
            if(file!=null){
                String format = Objects.requireNonNull(file.getOriginalFilename()).substring(file.getOriginalFilename().lastIndexOf("."));
                StoredFile storedFile = new StoredFile(storageService.store(file, format), format, uploadFile.getUsername());
                String fileId = storedFileRepository.save(storedFile).getId();
                fileResponseModel.setId(fileId + "");
                userService.updateUserByUsername(uploadFile.getUsername(), fileId);
                return ResponseEntity.ok().body(fileResponseModel);
            }else {
                return responseModel.responseModel(HttpStatus.BAD_REQUEST.value(), Collections.singletonList("Error in convert"));

            }

        } catch (Exception exception) {
            return responseModel.responseModel(HttpStatus.BAD_REQUEST.value(), Collections.singletonList(exception.getMessage()));
        }

    }

    public String findFileById(String id) {
        StoredFile storedFile = storedFileRepository.findById(id).get();
        return storedFile.getName() + storedFile.getFormat();
    }
    private MultipartFile convertBase64ToPdf(String fileString){
        File file = new File("F:\\StoreFiles\\StoreFiles\\src\\main\\java\\com\\example\\StoreFiles\\temp.pdf");


        try (FileOutputStream fos = new FileOutputStream(file); ) {
            // To be short I use a corrupted PDF string, so make sure to use a valid one if you want to preview the PDF file
            byte[] decoder = Base64.getDecoder().decode(fileString);
            fos.write(decoder);
            System.out.println("PDF File Saved");
            FileInputStream input = new FileInputStream(file);
            MultipartFile multipartFile = new MockMultipartFile("file",
                    file. getName(), "text/plain", IOUtils. toByteArray(input));
            return multipartFile;
        } catch (Exception e) {
            e.printStackTrace();

            return null;
        }
    }
}
