package com.example.StoreFiles;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.internal.common.classpath.ClassPathResolver;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.hibernate.sql.Template;
import org.junit.Assert;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Objects;

import static io.restassured.RestAssured.get;

public class UserIntegrationTest extends SpringDemoApplicationTests{
    private HttpHeaders headers;
    private String registerUrl;
    private String filesUrl;

    private String downloadFiles;


    @Given("Users can register in the system by entering their information \"/api/v1/users/register\"$")
    public void registerGiven() throws Throwable {
        registerUrl="http://localhost:8080//api/v1/users/register";
        System.out.println("Add URL:"+registerUrl);
    }
    @When("Set request HEADER")
    public void setHeaders(){
        headers=new HttpHeaders();
        headers.add("Accept","application/json");
        headers.add("Content-Type","application/json");
    }

    @And("Send a POST HTTP request")
    public void sendPostRequest(){
        String name="Mostafa moradi";
        String username="MST22232";
        String password="123456";
        String jsonBody="{\"name\":\"" + name + "\",\"username\":\"" + username + "\",\"password\":\"" + password + "\"}";
        System.out.println(jsonBody);
        HttpEntity<String>entity=new HttpEntity<String>(jsonBody,headers);
        this.restTemplate=new RestTemplate();
        this.userResponseEntity=this.restTemplate.postForEntity(registerUrl,entity,String.class);

    }

    @Then ("receive valid Response")
    public void verifyPostResponse(){
        String responseBody = Objects.requireNonNull(userResponseEntity.getBody()).toString();
        System.out.println("responseBody --->" + responseBody);

    }
    //-----------------------------------------

    @Given("I Set POST File service api endpoint \"/api/v1/files/upload\"$")
    public void setUrlForInsertFiles() throws Throwable {
        filesUrl="http://localhost:8080/api/v1/files/upload";
        System.out.println("Add URL:"+filesUrl);
    }
    @When("I Set request HEADER For File")
    public void setHeadersForInsertFiles(){

        headers=new HttpHeaders();
        headers.add("Accept","application/json");
        headers.add("Content-Type","application/json");
    }

    @And("Send a POST HTTP request For File")
    public void sendPostRequestForInsertFiles() throws IOException {
        String username="Karami3";
        String file=base64Sample();
        String jsonBody="{\"username\":\"" + username + "\",\"file\":\"" + file + "\"}";
        HttpEntity<String>entity=new HttpEntity<String>(jsonBody,headers);
        this.restTemplate=new RestTemplate();
        this.fileResponseEntity=this.restTemplate.postForEntity(filesUrl,entity,String.class);


    }

    @Then ("^I receive valid Response For File$")
    public void verifyPostResponseForInsertFiles(){
        String responseBody = Objects.requireNonNull(fileResponseEntity.getBody()).toString();
        System.out.println("responseBody --->" + responseBody);

    }



    //------------------------------------------



    @Given("I perform GET operation for \"/api/v1/files\"$")
    public void getOperation() throws Throwable {
        downloadFiles="http://localhost:8080/api/v1/files/62345b336b55094db68b0a5b?base64=true";
        System.out.println("Add URL:"+downloadFiles);
        headers=new HttpHeaders();
        headers.add("Accept","application/json");
        headers.add("Content-Type","application/json");
    }
    @And("^I perform GET operation for the username$")
    public void setParam(){
        String path=downloadFiles;
        System.out.println(path);
        downloadResponse = get(path);

    }

    @Then("^Http response code should be (\\d+)$")
    public void httpResponseCodeShouldBe(int statusCode) {
        Assert.assertEquals(downloadResponse.statusCode(), statusCode);
    }


    @And("می خواهم جیسون ببینم")
    public void iShouldSeeJsonResponseWithAListOfSymptoms() {
        System.out.println(downloadResponse.getBody().asString());
        Assert.assertEquals(200, downloadResponse.statusCode());
    }


    private String base64Sample() throws IOException {
        File file = new File("D:\\pdf_sample.pdf");
        return encodeFileToBase64Binary(file.getAbsolutePath());

    }
    private static String encodeFileToBase64Binary(String fileName) throws IOException {
        File file = new File(fileName);
        byte[] encoded = Base64.encodeBase64(FileUtils.readFileToByteArray(file));
        return new String(encoded, StandardCharsets.US_ASCII);
    }














}
