package com.example.StoreFiles;

import io.cucumber.spring.CucumberContextConfiguration;
import io.restassured.response.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.ResponseErrorHandler;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;

@CucumberContextConfiguration
@SpringBootTest(classes = StoreFilesApplication.class, webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
class SpringDemoApplicationTests {

	ResponseEntity userResponseEntity=null;
	ResponseEntity fileResponseEntity=null;

	Response downloadResponse =null;


	@Autowired
	protected RestTemplate restTemplate;


}
